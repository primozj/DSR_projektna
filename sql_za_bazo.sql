Create table Uporabnik(
    ID INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nickname varchar(30),
    PASSWORD VARCHAR(30) ,
    Role varchar(30) 
);

create TABLE exercise(
    ID INT PRIMARY KEY not null auto_increment,
    name varchar(30),
    rep_range_fk int
);
create table rep_range(
    ID INT PRIMARY KEY not null auto_increment,
    range_from INT,
    range_to INT
);

ALTER TABLE exercise
ADD CONSTRAINT FK_rep_range
FOREIGN KEY (rep_range_fk) REFERENCES rep_range(ID);



create table workout(
    ID INT PRIMARY KEY not null auto_increment,
    name varchar(30),
    difficulty INT

);

CREATE table workout_has_exercise(
    ID_workout INT,
    ID_exercise INT 
);

ALTER TABLE workout_has_exercise
ADD CONSTRAINT FK_ID_workout
FOREIGN KEY ( ID_workout) REFERENCES workout(ID);

ALTER TABLE workout_has_exercise
ADD CONSTRAINT FK_ID_exercise
FOREIGN KEY ( ID_exercise) REFERENCES exercise(ID);


Create TABLE food(
    ID INT PRIMARY KEY not null auto_increment,
    name varchar(30),
    protein INT,
    fats INT,
    carbs INT
);

create table meal(
    ID INT PRIMARY KEY not null auto_increment,
    name varchar(30),
    type ENUM ('zajtrk', 'kosilo', 'vecerja')    
);

create table meal_has_food(
    ID_meal INT,
    ID_food INT 
);

ALTER TABLE meal_has_food
ADD CONSTRAINT FK_ID_meal
FOREIGN KEY ( ID_meal) REFERENCES meal(ID);

ALTER TABLE meal_has_food
ADD CONSTRAINT FK_ID_food
FOREIGN KEY ( ID_food) REFERENCES food(ID);


INSERT INTO uporabnik (nickname, password, role) VALUES
('Buraz', 'Ribe123', 'basic'),
('Brat', 'Ribez123', 'basic'),
('Buki', 'Ribogojnica123', 'basic'),
('Pes', 'Kosti123', 'basic'),
('primoz', 'admin', 'admin'),
('vladimir', 'notadmin', 'basic');

Insert into rep_range (range_from, range_to) VALUES
(4,6),
(6,10),
(8,10),
(10,14),
(12,15);

insert into exercise (name, rep_range_fk) values
("Bench press", 1),
("Barbell squat", 4),
("Shoulder press", 5),
("Bicep curls", 2),
("Lat pulldown", 4);

insert into workout (name, difficulty) VALUES
("Push", 3),
("Legs", 5),
("Arms", 2),
("Pull", 4),
("8 hour arm", 5),
("Shoulders", 4);

insert into workout_has_exercise VALUES
(1,1),
(1,3),
(2,2),
(4,5),
(3,2);

insert into food (name, protein, fats, carbs) values 
("Pomfri", 1, 7, 10),
("Chicken", 15, 8, 20),
("Lasagna", 12, 15, 19),
("Rice", 4, 10, 11);

insert into meal (name, type) VALUES
("Proteinski zajtrk", "zajtrk"),
("Proteinsko kosilo", "kosilo"),
("Proteinska vecerja", "vecerja");

INSERT INTO meal_has_food VALUES
(1,2),
(1,4),
(2,1),
(3,2);

create table uporabnik_has_meal (
    ID_uporabnik INT,
    ID_meal INT
);


ALTER TABLE uporabnik_has_meal
ADD CONSTRAINT FK_ID_uporabnik
FOREIGN KEY ( ID_uporabnik) REFERENCES uporabnik(ID);

ALTER TABLE uporabnik_has_meal
ADD CONSTRAINT FK_ID_meal2
FOREIGN KEY ( ID_meal) REFERENCES  meal(ID);


insert into uporabnik_has_meal VALUES
(1,1),
(1,2),
(1,3),
(2,3),
(2,4),
(3,1),
(3,3),
(3,2);

create table uporabnik_has_workout(
    ID_uporabnik INT,
    ID_workout INT
);

ALTER TABLE uporabnik_has_workout
ADD CONSTRAINT FK_ID_uporabnik2
FOREIGN KEY ( ID_uporabnik) REFERENCES  uporabnik(ID);

ALTER TABLE uporabnik_has_workout
ADD CONSTRAINT FK_ID_workout2
FOREIGN KEY ( ID_workout) REFERENCES  workout(ID);

